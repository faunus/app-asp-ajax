-- === queries
-- 3.

SELECT
    FirstName, 
    LastName, 
    EmailAddress
FROM
    Client
ORDER BY
    LastName
ASC

-- ====
-- 4.

SELECT
    comp.CompanyName,
    comp.EmailAddress,
    (
        SELECT
            PhoneNumber
        FROM
            CompanyTelephone phone2
        WHERE
            phone2.TelephoneType = 0
        AND
            phone.Id = phone2.Id
    ),
    (
        SELECT
            Address1
        FROM
            CompanyAddress addr2
        WHERE
            addr2.AddressType = 0
        AND
            addr.Id = addr2.Id
    )
FROM
    Company comp,
    CompanyTelephone phone,
    CompanyAddress addr
WHERE
    comp.Id = phone.ContactId
AND
    comp.Id = addr.ContactId;
AND
    comp.id = '46da3375-463e-4ec3-adb0-1c8a00914190';

-- === 
-- 5.

SELECT
    comp.Id,
    comp.CompanyName
FROM
    Company comp
WHERE
     NOT EXISTS
    (
        SELECT
            CompanyId
        FROM
            Client cl
        WHERE
            comp.Id = cl.CompanyId
    )

-- ===
-- 6. 

SELECT
    cl.Id,
    cl.FirstName,
    cl.LastName
FROM
    Client cl
WHERE
    (
        SELECT COUNT(*)
        FROM
            ClientTelephone clt
        WHERE
            clt.ContactId = cl.Id
    ) = 1
-- ====
-- 7.

SELECT
    cl.Id,
    cl.FirstName,
    cl.LastName
FROM
    Client cl
WHERE
    (
        SELECT COUNT(*)
        FROM
            ClientTelephone clt
        WHERE
            clt.ContactId = cl.Id
    ) > 0
-- ===
-- 8.
SELECT
    FirstName + ' ' +  LastName AS "ContactName",
    EmailAddress
FROM
    Client
UNION
SELECT
    CompanyName AS "ContactName",
    EmailAddress
FROM
    Company
